let userScore = 0;
let computerScore = 0;

const userScore_span = document.getElementById("pontosUsuario");
const computerScore_span = document.getElementById("pontosComputador");
const scoreBoard_div = document.querySelector(".pontuacao");
const result_p = document.querySelector(".resultado > p");
const rock_div = document.getElementById("opcaoPedra");
const paper_div = document.getElementById("opcaoPapel");
const scissors_div = document.getElementById("opcaoTesoura");

function getComputerChoice() {

    const choices = ["opcaoPedra", "opcaoPapel", "opcaoTesoura"];
    const randomNumber = Math.floor(Math.random() * 3);
    return choices[randomNumber];
}

function converterPalavra(palavra) {

    if (palavra == "opcaoPedra") return "Pedra";
    if (palavra == "opcaoPapel") return "Papel";
    else return "Tesoura";
}

function UsuarioGanhou(userChoice, computerChoice) {

    userScore++;
    userScore_span.innerHTML = userScore;
    computerScore_span.innerHTML = computerScore;
    result_p.innerHTML = `${converterPalavra(userChoice)}${smallUserWord} ganha de ${converterPalavra(computerChoice)}${smallComputerWord}. Você venceu!`

}

function UsuarioPerdeu(userChoices, computerChoice) {

    computerScore++;
    userScore_span.innerHTML = userScore;
    computerScore_span.innerHTML = computerScore;
    result_p.innerHTML = `${converterPalavra(userChoice)}${smallUserWord} perde de  ${converterPalavra(computerChoice)}${smallComputerWord}. Você perdeu!`
}

function UsuarioEmpatou(userChoice, computerChoice) {

    userScore_span.innerHTML = userScore;
    computerScore_span.innerHTML = computerScore;
    result_p.innerHTML = `${converterPalavra(userChoice)}${smallUserWord} é igual à ${converterPalavra(computerChoice)}${smallComputerWord}. Vocês empataram!`
}

function game(userChoice) {

    const computerChoice = getComputerChoice();
    switch (userChoice + " " + computerChoice) {

        case "opcaoPedra opcaoTesoura":
        case "opcaoPapel opcaoPedra":
        case "opcaoTesoura opcaoPapel":
            UsuarioGanhou(userChoice, computerChoice);
            break;

        case "opcaoPedra opcaoPapel":
        case "opcaoPapel opcaoTesoura":
        case "opcaoTesoura opcaoPedra":
            UsuarioPerdeu(userChoice, computerChoice);
            break;

        case "opcaoPedra opcaoPedra":
        case "opcaoPapel opcaoPapel":
        case "opcaoTesoura opcaoTesoura":
            UsuarioEmpatou(userChoice, computerChoice);
            break;
    }
}

//função para eventos com os botões de seleção pedra, papel e tesoura.

function main() {
    
    rock_div.addEventListener('click', function () {
        game("opcaoPedra");
    })

    paper_div.addEventListener('click', function () {
        game("opcaoPapel")
    })

    scissors_div.addEventListener('click', function () {
        game("opcaoTesoura")
    })
}

main(); //chamei a função para o game rodar